# Ma formation Developpeur Web chez Simplon

### Bloc HTML/CSS

- Commandes de bases et structure de page:
  - Les balises structurantes : `<header>; <nav>; <main>; <section>; <article>; <aside>; <figure>; <footer>`
  - Les selecteurs CSS : les balises HTML; les class HTML représentées par un . ; les ID HTML repsrésentés par un #
- Selecteurs avancés CSS :
  - ex : nth-child; first-of-type; first-child; last-child; etc...  avec un mini-jeu [CSS Diner](https://flukeout.github.io/)
- Les propriétés de positionnement CSS:
  - **Flexbox** avec le jeu [Flexbox Froggy](https://flexboxfroggy.com/#fr)
    - `display : flex; flex-direction; flex-wrap; flex-flow; etc...`
  - **Grid** avec le jeu [CSS Grid garden](https://cssgridgarden.com/#fr)
    - `display : grid; grid-template-rows||columns; grid-row||columns`
- Le framework **Bootstrap** en faisant un exercice de reconstitution d'un epage web existante en utilisant Bootstrap.



Pour finir on finit par un projet de [portfolio](https://www.simplonlyon.fr/promo9/tsebastianelli/)



### Bloc Javascript



- Les bases avec les variables `let et const` et leur type :

  - les primitives:
    - boolean
    - string
    - number
    - null
    - undefined
    - infinity
  - les complexes:
    - function
    - array
    - object

- Les boucles `while; for`

- LE DOM: Résultat de l'interprétation du HTML par le navigateur. 

  On se sert du DOM pour manipuler des éléments HTML avec js (réaliser une action au clic, ...)

  JS ne change jamais le code source mais rajoute une couche de code au dessus du HTML et du CSS





